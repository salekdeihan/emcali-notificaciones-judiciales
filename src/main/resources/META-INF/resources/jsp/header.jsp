<div class="emcali-notificaciones-judiciales">
	<header>
		<div class="container">
			<div class="row">
				<div class="col-sm-4 col-xs-12">
					<img class="logo" alt="Emcali" src="<%=request.getContextPath() %>/images/layout_set_logo.png">
				</div>
				<div class="col-sm-8 col-xs-12">
					<h1>Notificiones judiciales</h1>
				</div>
			</div>
		</div>
	</header>
</div>