/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of the Liferay Enterprise
 * Subscription License ("License"). You may not use this file except in
 * compliance with the License. You can obtain a copy of the License by
 * contacting Liferay, Inc. See the License for the specific language governing
 * permissions and limitations under the License, including but not limited to
 * distribution rights of the Software.
 *
 *
 *
 */

package co.com.emcali.notificacionesjudiales.portlet.command;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCRenderCommand;
import com.liferay.portal.kernel.util.ParamUtil;

import java.util.List;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

import co.com.emcali.notificacionesjudiales.constants.NotificacionesJudicialesPortletKeys;
import co.com.emcali.notificacionesjudiales.model.NotificacionJudicial;
import co.com.emcali.notificacionesjudiales.utils.NotificacionesJudicialesUtil;

/**
 * @author salek
 */

@Component(
		immediate = true,
		property = {
				"javax.portlet.name=" + NotificacionesJudicialesPortletKeys.NOTIFICACIONESJUDICIALESADMIN,
				"mvc.command.name=/notificacionesjudiales/adminNotifications" },
		service = MVCRenderCommand.class)
public class AdminNotificationsMVCRenderCommand implements MVCRenderCommand {

	private Log _log = LogFactoryUtil.getLog(getClass());

	@Override
	public String render(RenderRequest renderRequest, RenderResponse renderResponse) {

		String documento = ParamUtil.getString(renderRequest, "documento");
		String tipoDocumento = ParamUtil.getString(renderRequest, "tipoDocumento");
		_log.info("documento : " + documento);

		List<NotificacionJudicial> notificacionesJudiciales = NotificacionesJudicialesUtil.findByDocumento(tipoDocumento,
				documento);

		renderRequest.setAttribute("notificacionesJudiciales", notificacionesJudiciales);

		_log.info("render adminNotifications");
		return "/jsp/admin/adminNotifications.jsp";
	}

}
