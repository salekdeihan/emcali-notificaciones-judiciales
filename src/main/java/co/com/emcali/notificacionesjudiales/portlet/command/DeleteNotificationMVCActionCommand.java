package co.com.emcali.notificacionesjudiales.portlet.command;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.util.ParamUtil;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;

import co.com.emcali.notificacionesjudiales.constants.NotificacionesJudicialesPortletKeys;
import co.com.emcali.notificacionesjudiales.utils.NotificacionesJudicialesUtil;

/**
 * @author salek
 */
@Component(
		immediate = true, 
		property = {
				"javax.portlet.name=" + NotificacionesJudicialesPortletKeys.NOTIFICACIONESJUDICIALES,
				"mvc.command.name=/notificacionesjudiales/deleteNotification" }, 
		service = MVCActionCommand.class)

public class DeleteNotificationMVCActionCommand extends BaseMVCActionCommand {

	private Log _log = LogFactoryUtil.getLog(DeleteNotificationMVCActionCommand.class);

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) {

		_log.info("tk : delete");
		int id = ParamUtil.getInteger(actionRequest, "id");
		if (!NotificacionesJudicialesUtil.deleteNotificacionJudicial(id)) {
			SessionErrors.add(actionRequest, "error-delete");
		}

		actionResponse.setRenderParameter("mvcRenderCommandName", "/notificacionesjudiales/userNotifications");

	}
}