<%@ include file="./../init.jsp"%>
<%@ include file="./../header.jsp"%>


<%
	String tipoDocumento = (String) request.getAttribute("tipoDocumento");
	String documento = (String) request.getAttribute("documento");
	
%>

<liferay-portlet:actionURL name="/notificacionesjudiales/searchConfirmation" var="searchConfirmation" />

<div class="emcali-notificaciones-judiciales">
	<section class="form-register">
		<form class="text-center" action="<%=searchConfirmation.toString()%>" method="post">
			<p class="mb"><liferay-ui:message key="emcali.notificacionesjudiciales.admin.buscar.confirmacion.codigo" /></p>
			<div class="input-wrap text-center">
				<div>
				<input type="hidden" value="<%=tipoDocumento %>" name="<portlet:namespace/>tipoDocumento" />
				<input type="hidden" value="<%=documento %>" name="<portlet:namespace/>documento" />
					<input type="text" name="<portlet:namespace />codigo" id="codigo" class="field" placeholder='<liferay-ui:message key="emcali.notificacionesjudiciales.admin.buscar.confirmacion.codigo" />'>
				</div>
			</div>
			<button class="botons"><liferay-ui:message key="emcali.notificacionesjudiciales.admin.buscar.confirmacion.continuar" /></button>
		</form>
	</section>
</div>