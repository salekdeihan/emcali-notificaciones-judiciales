<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@ include file="./../init.jsp"%>
<%@ include file="./../header.jsp"%>
<script src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
<script src="<%=request.getContextPath()%>/js/validacionNewNotificacion.js"></script>

<liferay-portlet:actionURL name="/notificacionesjudiales/editNotification" var="editNotification" />

<%
	NotificacionJudicial notificacionJudicial = (NotificacionJudicial) request.getAttribute("notificacionJudicial");
	String tipoPersonaNaturalChecked = "";
	String tipoPersonaJuridiaChecked = "";
	String tipoPersona = notificacionJudicial.getTipoPersona();

	if(tipoPersona.equals(Constantes.TIPO_PERSONA_NATURAL)){
		tipoPersonaNaturalChecked = "checked=\"checked\"";
	}else{
		tipoPersonaJuridiaChecked = "checked=\"checked\"";
	}
	
	String tipoDocumentoCedulaChecked = "";
	String tipoDocumentoNitChecked = "";

	String tipoDocumento = notificacionJudicial.getTipoDocumento();
	if(tipoDocumento.equals(Constantes.TIPO_DOCUMENTO_CEDULA)){
		tipoDocumentoCedulaChecked = "checked=\"checked\"";
	}else{
		tipoDocumentoNitChecked = "checked=\"checked\"";
	}
	
	
%>


<div class="emcali-notificaciones-judiciales">
	<liferay-ui:error key="error-add" message="emcali.notificacionesjudiciales.user.add.error-add" />
	<section>
		<div class="container">
			<form action="<%=editNotification.toString()%>" method="post" class="form-container" name="form" enctype="application/x-www-form-urlencoded">
				<input type="hidden" name='<portlet:namespace/>id' value="<%=notificacionJudicial.getId()%>"/>
				<br />
				<div class="row">
					<div class="col-sm-12">
						<span class="mb d-block"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.tipo-persona" /></span>
						<div class="d-flex align-items-center">
							<label class="container-radio mr"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.tipo-natural" />
								<input <%=tipoPersonaNaturalChecked %> value="<%=Constantes.TIPO_PERSONA_NATURAL %>" type="radio" name="<portlet:namespace />tipoPersona" /><span class="checkmark"></span>
							</label>
							<label class="container-radio"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.tipo-juridico" /> 
								<input <%=tipoPersonaJuridiaChecked %> value="<%=Constantes.TIPO_PERSONA_JURIDICO %>" type="radio" name="<portlet:namespace />tipoPersona" />
								<span class="checkmark"></span>
							</label>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="input-wrap">
							<label for="personType"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.nombre" /></label> 
							<input value="<%=notificacionJudicial.getNombre() %>" id="nombre" type="text" onchange="return valida_envia()" name="<portlet:namespace />nombre" />
						</div>
					</div>
					<div class="col-sm-6">
						<div class="input-wrap">
							<label for="personType"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.apellido" /></label>
							<input value="<%=notificacionJudicial.getApellido() %>" id="personType" type="text" name="<portlet:namespace />apellido" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="input-wrap">
							<label for="personType"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.razon-social" /></label> 
							<input value="<%=notificacionJudicial.getRazonSocial() %>" id="personType" type="text"	name="<portlet:namespace />razonSocial" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<span class="mb d-block"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.tipo-documento" /></span>
						<div class="d-flex align-items-center">
							<label class="container-radio mr"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.tipo-nit" />
								<input <%=tipoDocumentoNitChecked %> value="<%=Constantes.TIPO_DOCUMENTO_NIT %>" type="radio" name="<portlet:namespace />tipoDocumento"> <span class="checkmark"></span>
							</label> 
							<label class="container-radio"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.tipo-cedula" />
								<input <%=tipoDocumentoCedulaChecked %> value="<%=Constantes.TIPO_DOCUMENTO_CEDULA %>" type="radio" name="<portlet:namespace />tipoDocumento"><span class="checkmark"></span>
							</label>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="input-wrap">
							<label for="personType"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.numero-documento"/></label> 
							<input value="<%=notificacionJudicial.getNumeroIdentificacion() %>" type="number" maxlength='10' id="idNumber" name="<portlet:namespace />numeroDocument" //>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="input-wrap">
							<label for="personType"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.numero-acto-administrativo"/></label>
							<input value="<%=notificacionJudicial.getActoNumero() %>" maxlength="10" id="personType" type="number" name="<portlet:namespace />numeroActoAdministrativo" />
						</div>
					</div>
					<div class="col-sm-6">
						<div class="input-wrap">
							<label for="adminActName"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.nombre-acto-administrativo"/></label>
							<input value="<%=notificacionJudicial.getActoNombre() %>" id="adminActName" type="text" name="<portlet:namespace />nombreActoAdministrativos" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="input-wrap">
							<label for="personType"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.especificar"/></label>
							<input value="<%=notificacionJudicial.getEspecificar() %>" id="personType" type="text" name="<portlet:namespace />especificar" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="input-wrap">
							<label for="personType"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.fecha-acto-administrativo"/></label>
							<input value="<%=sdfInputDateValue.format(notificacionJudicial.getActoFecha()) %>" id="date" type="date" name="<portlet:namespace />fechaActoAdministrativo">
						</div>
					</div>
					<div class="col-sm-6">
						<div class="input-wrap">
							<label for="contractNumber"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.numero-contrato"/></label> 
							<input value="<%=notificacionJudicial.getContrato() %>" type="number" id="contractNumber" maxlength="10" name="<portlet:namespace />numeroContrato">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="input-wrap">
							<label for="personType"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.fecha-publicacion"/></label>
							<input value="<%=sdfInputDateValue.format(notificacionJudicial.getFechaInicio()) %>" maxlength="10" id="personType" type="date" name="<portlet:namespace />fechaInicioPublication">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="input-wrap">
							<label for="personType"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.numero-radicado"/></label>
							<input value="<%=notificacionJudicial.getNumeroRadicado() %>" maxlength="10" id="personType" type="text" name="<portlet:namespace />numeroRadicado">
						</div>
					</div>
					<div class="col-sm-6">
						<div class="input-wrap">
							<label for="adminActName"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.dependencia-acto"/></label>
							<input value="<%=notificacionJudicial.getDependencia() %>" id="adminActName" type="text" name="<portlet:namespace />adminActName" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="input-wrap">
							<label for="phone"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.celular"/></label> 
							<input value="<%=notificacionJudicial.getTelefono() %>" maxlength="10" id="phone" type="number" name="<portlet:namespace />celular">
						</div>
					</div>
					<div class="col-sm-6">
						<div class="input-wrap">
							<label for="email"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.email"/></label>
							<input value="<%=notificacionJudicial.getEmail() %>" autocomplete="off" id="email" type="email" data-validation="email" onchange="return mailValidation()" name="<portlet:namespace />email" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="input-wrap">
							<label for="observations"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.observaciones"/></label>
							<textarea value="<%=notificacionJudicial.getObservaciones() %>" id="observations" cols="30" rows="5" name="<portlet:namespace />observaciones"><%=notificacionJudicial.getObservaciones() %></textarea>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="input-wrap">
							<label for="publicationPermanent"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.publicacion-permanente"/></label>
							<input value="<%=notificacionJudicial.getPermanente() %>" id="publicationPermanent" type="text" name="<portlet:namespace />publicacionPermanente" />
						</div>
					</div>
					<div class="col-sm-6">
						<div class="input-wrap">
							<label for="state"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.estado"/></label>
							<select id="state" name="<portlet:namespace />estado">
								<option value="Activo"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.activo"/></option>
								<option value="Inactivo"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.inactivo"/></option>
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="input-wrap">
							<label for="document">
								<%if(Validator.isNotNull(notificacionJudicial.getAnexo()) && !notificacionJudicial.getAnexo().isEmpty()){ %>
									<a href="data:application/pdf;base64,<%=notificacionJudicial.getAnexo() %>" download="anexo.pdf">
										<liferay-ui:message key="emcali.notificacionesjudiciales.user.add.documento-anexo"/>
									</a>
								<%}else{ %>
										<liferay-ui:message key="emcali.notificacionesjudiciales.user.add.documento-anexo"/>
								<%} %>
							</label>
							<input id="file" type="file" accept="application/pdf" onchange="return fileValidation()" name="<portlet:namespace />documentoAnexo" />
						</div>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-sm-12">
						<button class="app-btn"><liferay-ui:message key="emcali.notificacionesjudiciales.general.guardar"/></button>
					</div>
				</div>
				<br> <br>
			</form>
		</div>
	</section>
</div>
