<%@ include file="./../init.jsp"%>
<%@ include file="./../header.jsp"%>

<liferay-portlet:renderURL var="searchConfirmation">
	<liferay-portlet:param name="mvcRenderCommandName" value="/notificacionesjudiales/searchConfirmation" />
</liferay-portlet:renderURL>

<div class="emcali-notificaciones-judiciales">
	<liferay-ui:error key="error-search" message="emcali.notificacionesjudiciales.admin.buscar.notificacion.busqueda-error" />
	<section class="form-register">
		<form class="text-center" action="<%=searchConfirmation.toString()%>" method="post">
			<div class="input-wrap text-left">
				<label><liferay-ui:message key="emcali.notificacionesjudiciales.admin.buscar.notificacion.busqueda-por" /></label>
				<div class="d-flex">
					<select name="<portlet:namespace />tipoDocumento" id="" class="mr">
						<option value="0"><liferay-ui:message key="emcali.notificacionesjudiciales.admin.buscar.notificacion.cedula" /></option>
						<option value="1"><liferay-ui:message key="emcali.notificacionesjudiciales.admin.buscar.notificacion.contrato" /></option>
						<option value="2"><liferay-ui:message key="emcali.notificacionesjudiciales.admin.buscar.notificacion.acto" /></option>
					</select> 
					<input type="text" name="<portlet:namespace />documento" class="field" placeholder='<liferay-ui:message key="emcali.notificacionesjudiciales.admin.buscar.notificacion.numero" />'>
				</div>
			</div>
			<button class="botons" id="buscar"><liferay-ui:message key="emcali.notificacionesjudiciales.admin.buscar.notificacion.buscar" /></button>
		</form>
	</section>
</div>