<%@ include file="./../init.jsp"%>
<%@ include file="./../header.jsp"%>
<script src="<%=request.getContextPath()%>/js/validacionLogin.js"></script>

<liferay-portlet:actionURL name="/notificacionesjudiales/login" var="login" />
<div class="emcali-notificaciones-judiciales">
	<liferay-ui:error key="error-login" message="emcali.notificacionesjudiciales.user.login.error-login" />
	<section class="form-register">
		<form action="<%=login.toString()%>" method="post" class="text-center" id="formulario">
			<label class="mb"><liferay-ui:message key="emcali.notificacionesjudiciales.user.login.ingrese-usuario" /></label>
			<div class="input-wrap">
				<input type="text" id="usuario" name="<portlet:namespace />nombre" class="field" placeholder='<liferay-ui:message key="emcali.notificacionesjudiciales.user.login.nombre" />'>
				<input type="password" id="contraseņa" name="<portlet:namespace />pass" class="field" placeholder='<liferay-ui:message key="emcali.notificacionesjudiciales.user.login.password" />'>
			</div>
			<button class="botons" id="acceder"><liferay-ui:message key="emcali.notificacionesjudiciales.user.login.acceder" /></button>
		</form>
	</section>
</div>