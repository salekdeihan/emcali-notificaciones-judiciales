package co.com.emcali.notificacionesjudiales.portlet.command;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;

import co.com.emcali.notificacionesjudiales.constants.NotificacionesJudicialesPortletKeys;
import co.com.emcali.notificacionesjudiales.model.NotificacionJudicial;
import co.com.emcali.notificacionesjudiales.utils.NotificacionesJudicialesUtil;

/**
 * @author salek
 */
@Component(
		immediate = true, 
		property = {
				"javax.portlet.name=" + NotificacionesJudicialesPortletKeys.NOTIFICACIONESJUDICIALES,
				"mvc.command.name=/notificacionesjudiales/editNotification" }, 
		service = MVCActionCommand.class)

public class EditNotificationMVCActionCommand extends BaseMVCActionCommand {

	private Log _log = LogFactoryUtil.getLog(EditNotificationMVCActionCommand.class);

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

		int id = ParamUtil.getInteger(actionRequest, "id");
		String nombre = ParamUtil.getString(actionRequest, "nombre");
		String apellido = ParamUtil.getString(actionRequest, "apellido");
		String razonSocial = ParamUtil.getString(actionRequest, "razonSocial");
		String tipoPersona = ParamUtil.getString(actionRequest, "tipoPersona");
		String tipoDocumento = ParamUtil.getString(actionRequest, "tipoDocumento");
		Integer numeroDocumento = ParamUtil.getInteger(actionRequest, "numeroDocument");
		String numeroActoAdministrativo = ParamUtil.getString(actionRequest, "numeroActoAdministrativo");
		String nombreActoAdministrativos = ParamUtil.getString(actionRequest, "nombreActoAdministrativos");
		Date fechaActoAdministrativo = ParamUtil.getDate(actionRequest, "fechaActoAdministrativo", sdf);
		String especificar = ParamUtil.getString(actionRequest, "especificar");
		String numeroContrato = ParamUtil.getString(actionRequest, "numeroContrato");
		Date fechaInicioPublication = ParamUtil.getDate(actionRequest, "fechaInicioPublication", sdf);
		String numeroRadicado = ParamUtil.getString(actionRequest, "numeroRadicado");
		String dependencia = ParamUtil.getString(actionRequest, "adminActName");
		String celular = ParamUtil.getString(actionRequest, "celular");
		String email = ParamUtil.getString(actionRequest, "email");
		String observaciones = ParamUtil.getString(actionRequest, "observaciones");
		String publicacionPermanente = ParamUtil.getString(actionRequest, "publicacionPermanente");
		String estado = ParamUtil.getString(actionRequest, "estado");
		UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(actionRequest);
		File file = uploadPortletRequest.getFile("documentoAnexo");
		
		NotificacionJudicial notificacionJudicial = NotificacionesJudicialesUtil.findNotificacionJudicialById(id);
		
		notificacionJudicial.setNombre(nombre);
		notificacionJudicial.setApellido(apellido);
		notificacionJudicial.setRazonSocial(razonSocial);
		notificacionJudicial.setTipoDocumento(tipoDocumento);
		notificacionJudicial.setTipoPersona(tipoPersona);
		notificacionJudicial.setNumeroIdentificacion(numeroDocumento);
		notificacionJudicial.setEspecificar(especificar);
		notificacionJudicial.setActoFecha(fechaActoAdministrativo);
		notificacionJudicial.setActoNumero(numeroActoAdministrativo);
		notificacionJudicial.setActoNombre(nombreActoAdministrativos);
		notificacionJudicial.setFechaInicio(fechaInicioPublication);
		notificacionJudicial.setNumeroRadicado(numeroRadicado);
		notificacionJudicial.setDependencia(dependencia);
		notificacionJudicial.setPermanente(publicacionPermanente);
		notificacionJudicial.setEmail(email);
		notificacionJudicial.setEstado(estado);
		notificacionJudicial.setTelefono(celular);
		notificacionJudicial.setObservaciones(observaciones);
		notificacionJudicial.setContrato(numeroContrato);
		notificacionJudicial.setAnexo(NotificacionesJudicialesUtil.getBase64File(file));

		if (NotificacionesJudicialesUtil.editNotificacionJudicial(notificacionJudicial)) {
			actionResponse.setRenderParameter("mvcRenderCommandName", "/notificacionesjudiales/userNotifications");
		} else {
			SessionErrors.add(actionRequest, "error-add");
			actionRequest.setAttribute("notificacionJudicial", notificacionJudicial);
			actionResponse.setRenderParameter("mvcRenderCommandName", "/notificacionesjudiales/editNotification");
		}
	}

}