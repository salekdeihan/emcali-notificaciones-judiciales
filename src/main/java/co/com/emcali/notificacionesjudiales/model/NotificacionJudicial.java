package co.com.emcali.notificacionesjudiales.model;

import java.io.Serializable;

public class NotificacionJudicial implements Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	private Integer numeroIdentificacion;
	private String nombre;
	private String razonSocial;
	private String numeroRadicado;
	private String tipoPersona;
	private String tipoDocumento;
	private String apellido;
	private String actoNumero;
	private String actoNombre;
	private java.util.Date actoFecha;
	private String contrato;
	private java.util.Date fechaInicio;
	private java.util.Date fechaFin;
	private String dependencia;
	private String observaciones;
	private String permanente;
	private String telefono;
	private String email;
	private String anexo;
	private String estado;
	private String especificar;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getNumeroIdentificacion() {
		return numeroIdentificacion;
	}

	public void setNumeroIdentificacion(Integer numeroIdentificacion) {
		this.numeroIdentificacion = numeroIdentificacion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getRazonSocial() {
		return razonSocial;
	}

	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}

	public String getNumeroRadicado() {
		return numeroRadicado;
	}

	public void setNumeroRadicado(String numeroRadicado) {
		this.numeroRadicado = numeroRadicado;
	}

	public String getTipoPersona() {
		return tipoPersona;
	}

	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getActoNumero() {
		return actoNumero;
	}

	public void setActoNumero(String actoNumero) {
		this.actoNumero = actoNumero;
	}

	public String getActoNombre() {
		return actoNombre;
	}

	public void setActoNombre(String actoNombre) {
		this.actoNombre = actoNombre;
	}

	public java.util.Date getActoFecha() {
		return actoFecha;
	}

	public void setActoFecha(java.util.Date actoFecha) {
		this.actoFecha = actoFecha;
	}

	public String getContrato() {
		return contrato;
	}

	public void setContrato(String contrato) {
		this.contrato = contrato;
	}

	public java.util.Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(java.util.Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public java.util.Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(java.util.Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public String getDependencia() {
		return dependencia;
	}

	public void setDependencia(String dependencia) {
		this.dependencia = dependencia;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getPermanente() {
		return permanente;
	}

	public void setPermanente(String Permanente) {
		this.permanente = Permanente;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAnexo() {
		return anexo;
	}

	public void setAnexo(String anexo) {
		this.anexo = anexo;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getEspecificar() {
		return especificar;
	}

	public void setEspecificar(String especificar) {
		this.especificar = especificar;
	}

}
