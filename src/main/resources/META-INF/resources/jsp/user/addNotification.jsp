<%@ include file="./../init.jsp"%>
<%@ include file="./../header.jsp"%>
<script src="<%=request.getContextPath()%>/js/jquery.validate.js"></script>
<script
	src="<%=request.getContextPath()%>/js/validacionNewNotificacion.js"></script>

<liferay-portlet:actionURL
	name="/notificacionesjudiales/saveNotification" var="saveNotification" />

<div class="emcali-notificaciones-judiciales">
	<liferay-ui:error key="error-add" message="emcali.notificacionesjudiciales.user.add.error-add" />

	<section>
		<div class="container">
			<form action="<%=saveNotification.toString()%>" method="post" class="form-container" name="form" enctype="application/x-www-form-urlencoded">
				<br>
				<div class="row">
					<div class="col-sm-12">
						<span class="mb d-block"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.tipo-persona" /></span>
						<div class="d-flex align-items-center">
							<label class="container-radio mr"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.tipo-natural" />
								<input value="<%=Constantes.TIPO_PERSONA_NATURAL %>" type="radio" name="<portlet:namespace />tipoPersona" /><span class="checkmark"></span>
							</label>
							<label class="container-radio"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.tipo-juridico" /> 
								<input value="<%=Constantes.TIPO_PERSONA_JURIDICO %>" type="radio" name="<portlet:namespace />tipoPersona" />
								<span class="checkmark"></span>
							</label>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="input-wrap">
							<label for="personType"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.nombre" /></label> 
							<input id="nombre" type="text" onchange="return valida_envia()" name="<portlet:namespace />nombre" />
						</div>
					</div>
					<div class="col-sm-6">
						<div class="input-wrap">
							<label for="personType"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.apellido" /></label>
							<input id="personType" type="text" name="<portlet:namespace />apellido" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="input-wrap">
							<label for="personType"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.razon-social" /></label> 
							<input id="personType" type="text"	name="<portlet:namespace />razonSocial" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<span class="mb d-block"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.tipo-documento" /></span>
						<div class="d-flex align-items-center">
							<label class="container-radio mr"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.tipo-nit" />
								<input value="<%=Constantes.TIPO_DOCUMENTO_NIT %>" type="radio" name="<portlet:namespace />tipoDocumento"> <span class="checkmark"></span>
							</label> 
							<label class="container-radio"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.tipo-cedula" />
								<input value="<%=Constantes.TIPO_DOCUMENTO_CEDULA %>" type="radio" name="<portlet:namespace />tipoDocumento"><span class="checkmark"></span>
							</label>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="input-wrap">
							<label for="personType"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.numero-documento"/></label> 
							<input type="number" maxlength='10' id="idNumber" name="<portlet:namespace />numeroDocument" //>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="input-wrap">
							<label for="personType"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.numero-acto-administrativo"/></label>
							<input maxlength="10" id="personType" type="number" name="<portlet:namespace />numeroActoAdministrativo" />
						</div>
					</div>
					<div class="col-sm-6">
						<div class="input-wrap">
							<label for="adminActName"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.nombre-acto-administrativo"/></label>
							<input id="adminActName" type="text" name="<portlet:namespace />nombreActoAdministrativos" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="input-wrap">
							<label for="personType"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.especificar"/></label>
							<input id="personType" type="text" name="<portlet:namespace />especificar" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="input-wrap">
							<label for="personType"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.fecha-acto-administrativo"/></label>
							<input id="date" type="date" name="<portlet:namespace />fechaActoAdministrativo">
						</div>
					</div>
					<div class="col-sm-6">
						<div class="input-wrap">
							<label for="contractNumber"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.numero-contrato"/></label> 
							<input type="number" id="contractNumber" maxlength="10" name="<portlet:namespace />numeroContrato">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="input-wrap">
							<label for="personType"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.fecha-publicacion"/></label>
							<input maxlength="10" id="personType" type="date" name="<portlet:namespace />fechaInicioPublication">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="input-wrap">
							<label for="personType"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.numero-radicado"/></label>
							<input maxlength="10" id="personType" type="text" name="<portlet:namespace />numeroRadicado">
						</div>
					</div>
					<div class="col-sm-6">
						<div class="input-wrap">
							<label for="adminActName"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.dependencia-acto"/></label>
							<input id="adminActName" type="text" name="<portlet:namespace />adminActName" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="input-wrap">
							<label for="phone"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.celular"/></label> 
							<input maxlength="10" id="phone" type="number" name="<portlet:namespace />celular">
						</div>
					</div>
					<div class="col-sm-6">
						<div class="input-wrap">
							<label for="email"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.email"/></label>
							<input autocomplete="off" id="email" type="email" data-validation="email" onchange="return mailValidation()" name="<portlet:namespace />email" />
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="input-wrap">
							<label for="observations"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.observaciones"/></label>
							<textarea id="observations" cols="30" rows="5" name="<portlet:namespace />observaciones"></textarea>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="input-wrap">
							<label for="publicationPermanent"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.publicacion-permanente"/></label>
							<input id="publicationPermanent" type="text" name="<portlet:namespace />publicacionPermanente" />
						</div>
					</div>
					<div class="col-sm-6">
						<div class="input-wrap">
							<label for="state"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.estado"/></label>
							<select id="state" name="<portlet:namespace />estado">
								<option value="Activo"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.activo"/></option>
								<option value="Inactivo"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.inactivo"/></option>
							</select>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="input-wrap">
							<label for="document"><liferay-ui:message key="emcali.notificacionesjudiciales.user.add.documento-anexo"/></label>
							<input id="file" type="file" accept="application/pdf" onchange="return fileValidation()" name="<portlet:namespace />documentoAnexo" />
						</div>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-sm-12">
						<button class="app-btn"><liferay-ui:message key="emcali.notificacionesjudiciales.general.guardar"/></button>
					</div>
				</div>
				<br> <br>
			</form>
		</div>
	</section>
</div>