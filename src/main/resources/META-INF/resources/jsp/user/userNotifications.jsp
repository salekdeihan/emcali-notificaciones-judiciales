<%@ include file="./../init.jsp"%>
<%@ include file="./../header.jsp"%>

<%
	List<NotificacionJudicial> notificacionesJudiciales = (List<NotificacionJudicial>) request.getAttribute("notificacionesJudiciales");
	
%>

<liferay-portlet:renderURL var="addNotification">
	<liferay-portlet:param name="mvcRenderCommandName" value="/notificacionesjudiales/addNotification" />
</liferay-portlet:renderURL>

<liferay-portlet:renderURL var="userNotifications">
	<liferay-portlet:param name="mvcRenderCommandName" value="/notificacionesjudiales/userNotifications" />
</liferay-portlet:renderURL>

<div class="emcali-notificaciones-judiciales">
	<liferay-ui:error key="error-delete" message="emcali.notificacionesjudiciales.user.add.error-delete" />
	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-xs-12">
					<div class="input-wrap">
						<form action="<%=userNotifications.toString() %>" method="post">
							<input name='<portlet:namespace/>valorBusqueda' type="text" placeholder='<liferay-ui:message key="emcali.notificacionesjudiciales.user.listado.buscar" />'>
						</form>
					</div>
				</div>
				<div class="col-sm-6 col-xs-12">
					<a class="app-btn" href="<%=addNotification.toString()%>"><liferay-ui:message key="emcali.notificacionesjudiciales.user.listado.add" /></a>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="table-responsive">
						<table id="tablaNotificacion" class="table table-striped">
							<thead>
								<tr>
									<th><liferay-ui:message key="emcali.notificacionesjudiciales.user.listado.header.fecha" /></th>
									<th><liferay-ui:message key="emcali.notificacionesjudiciales.user.listado.header.id" /></th>
									<th><liferay-ui:message key="emcali.notificacionesjudiciales.user.listado.header.razon-social" /></th>
									<th><liferay-ui:message key="emcali.notificacionesjudiciales.user.listado.header.acto" /></th>
									<th><liferay-ui:message key="emcali.notificacionesjudiciales.user.listado.header.contrato" /></th>
									<th><liferay-ui:message key="emcali.notificacionesjudiciales.user.listado.header.estado" /></th>
									<th></th>
								</tr>
							</thead>
							<tbody>
							<%
								for (NotificacionJudicial notificacionJudicial : notificacionesJudiciales){
							%>		
									<liferay-portlet:actionURL name="/notificacionesjudiales/deleteNotification" var="deleteNotification">
										<liferay-portlet:param name="id" value="<%=String.valueOf(notificacionJudicial.getId()) %>"/>
									</liferay-portlet:actionURL>
									<liferay-portlet:renderURL var="editNotification">
										<liferay-portlet:param name="id" value="<%=String.valueOf(notificacionJudicial.getId()) %>"/>
										<liferay-portlet:param name="mvcRenderCommandName" value="/notificacionesjudiales/editNotification" />
									</liferay-portlet:renderURL>
									
									<tr>
										<td><%=sdf.format(notificacionJudicial.getActoFecha()) %></td>
										<td><%=notificacionJudicial.getNumeroIdentificacion() %></td>
										<td><%=notificacionJudicial.getNombre() %></td>
										<td><%=notificacionJudicial.getActoNumero() %></td>
										<td><%=notificacionJudicial.getContrato() %></td>
										<td><%=notificacionJudicial.getEstado() %></td>
										<td>
											<a href="<%=editNotification.toString()%>"> <span class="glyphicon glyphicon-edit" /></a>
											<%if(notificacionJudicial.getEstado().equals("Activo")){ %>
												<a href="<%=deleteNotification.toString()%>"><span class="glyphicon glyphicon-trash" /></a>
											<%} %>
										</td>
									</tr>
							<%		
								}
							%>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>