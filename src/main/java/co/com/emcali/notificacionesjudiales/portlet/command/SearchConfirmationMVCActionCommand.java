package co.com.emcali.notificacionesjudiales.portlet.command;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.util.ParamUtil;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;

import org.osgi.service.component.annotations.Component;

import co.com.emcali.notificacionesjudiales.constants.NotificacionesJudicialesPortletKeys;
import co.com.emcali.notificacionesjudiales.utils.NotificacionesJudicialesUtil;

/**
 * @author salek
 */
@Component(
		immediate = true, 
		property = {
				"javax.portlet.name=" + NotificacionesJudicialesPortletKeys.NOTIFICACIONESJUDICIALESADMIN,
				"mvc.command.name=/notificacionesjudiales/searchConfirmation" }, 
		service = MVCActionCommand.class)

public class SearchConfirmationMVCActionCommand extends BaseMVCActionCommand {

	private Log _log = LogFactoryUtil.getLog(SearchConfirmationMVCActionCommand.class);

	@Override
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) {
		_log.info("searchConfirmation");

		String tipoDocumento = ParamUtil.getString(actionRequest, "tipoDocumento");
		String documento = ParamUtil.getString(actionRequest, "documento");
		String codigo = ParamUtil.getString(actionRequest, "codigo");

		if (NotificacionesJudicialesUtil.validateCode(tipoDocumento, documento, codigo)) {
			_log.info("searchadmininotification");
			actionRequest.setAttribute("tipoDocumento", tipoDocumento);
			actionRequest.setAttribute("documento", documento);
			actionResponse.setRenderParameter("mvcRenderCommandName", "/notificacionesjudiales/adminNotifications");
		} else {
			actionResponse.setRenderParameter("mvcRenderCommandName", "/");
		}
	}

}