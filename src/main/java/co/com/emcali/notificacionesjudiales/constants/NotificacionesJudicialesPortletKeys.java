package co.com.emcali.notificacionesjudiales.constants;

/**
 * @author salek
 */
public class NotificacionesJudicialesPortletKeys {

	public static final String NOTIFICACIONESJUDICIALES =
			"co_com_emcali_notificacionesjudiales_NotificacionesJudicialesPortlet";

	public static final String NOTIFICACIONESJUDICIALESADMIN =
			"co_com_emcali_notificacionesjudiales_NotificacionesJudicialesAdminPortlet";

}