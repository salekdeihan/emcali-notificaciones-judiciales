<%@page import="java.text.SimpleDateFormat"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %><%@
taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %><%@
taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %><%@
taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<liferay-theme:defineObjects />
<%@page import="co.com.emcali.notificacionesjudiciales.user.model.NotificacionJudicial"%>
<%@page import="co.com.emcali.notificacionesjudiciales.user.utils.NotificacionesJudicialesUtil"%>
<%@page import="java.util.List"%>
<%@page import="co.com.emcali.notificacionesjudialesuser.constants.Constantes"%>

<portlet:defineObjects />

<%
	SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
	SimpleDateFormat sdfInputDateValue = new SimpleDateFormat("yyyy-MM-dd");

%>