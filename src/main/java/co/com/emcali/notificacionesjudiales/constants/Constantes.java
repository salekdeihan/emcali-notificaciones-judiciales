package co.com.emcali.notificacionesjudiales.constants;

/**
 * 
 * @author viventic
 *
 */
public class Constantes {

	public static final String WS_SERVIDOR_PRODUCCION = "http://172.18.1.28:9080/servicios-creg/rest/creg";

	public static final String WS_SERVIDOR_PRUEBAS = "http://172.18.30.114:9081/servicios-creg/rest/creg";

	public static final String WS_SERVIDOR_DEV = "http://ec2-3-231-205-86.compute-1.amazonaws.com:8080/api";

	public static final String WS_CONSUMO_SERVICIOS = WS_SERVIDOR_DEV;

	public static final String URL_CONSUMO_CONSULTA_FINDBYID = WS_CONSUMO_SERVICIOS + "/notificaciones";
	public static final String URL_CONSUMO_CONSULTA_FINDALL = WS_CONSUMO_SERVICIOS + "/notificaciones/findAll";
	public static final String URL_CONSUMO_CONSULTA_ADD = WS_CONSUMO_SERVICIOS + "/notificaciones/add";
	public static final String URL_CONSUMO_CONSULTA_UPDATE = WS_CONSUMO_SERVICIOS + "/notificaciones/alter";
	public static final String URL_CONSUMO_CONSULTA_DELETE = WS_CONSUMO_SERVICIOS + "/notificaciones/inactive";
	public static final String URL_CONSUMO_CONSULTA_FINDBYNOMBE = WS_CONSUMO_SERVICIOS + "/notificaciones/nombre";
	public static final String URL_CONSUMO_CONSULTA_FINDBYIDENTIFICADOR = WS_CONSUMO_SERVICIOS + "/notificaciones/identificacion";
	public static final String URL_CONSUMO_CONSULTA_FINDBYDOCUMENTO = WS_CONSUMO_SERVICIOS + "/notificaciones";

	public static final String URL_CONSULTA_EXTERNOS_GENERATE = WS_CONSUMO_SERVICIOS + "/externos/generate";
	public static final String URL_CONSULTA_EXTERNOS_VALIDATE = WS_CONSUMO_SERVICIOS + "/externos/validate";

	public static final String TIPO_PERSONA_NATURAL = "0";
	public static final String TIPO_PERSONA_JURIDICO = "1";
	public static final String TIPO_DOCUMENTO_NIT = "0";
	public static final String TIPO_DOCUMENTO_CEDULA = "1";

}
