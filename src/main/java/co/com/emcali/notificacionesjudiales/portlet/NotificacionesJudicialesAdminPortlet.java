package co.com.emcali.notificacionesjudiales.portlet;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;

import co.com.emcali.notificacionesjudiales.constants.NotificacionesJudicialesPortletKeys;

/**
 * @author salek
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=EMCALI",
		"com.liferay.portlet.header-portlet-css=/css/main.css",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=Notificaciones Judiciales User",
		"javax.portlet.init-param.template-path=/jsp/",
		"javax.portlet.init-param.view-template=/jsp/searchNotification.jsp",
		"javax.portlet.name=" + NotificacionesJudicialesPortletKeys.NOTIFICACIONESJUDICIALESADMIN,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class NotificacionesJudicialesAdminPortlet extends MVCPortlet {

}