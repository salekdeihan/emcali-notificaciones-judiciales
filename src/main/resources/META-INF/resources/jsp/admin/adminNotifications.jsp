<%@ include file="./../init.jsp"%>
<%@ include file="./../header.jsp"%>


<%
	List<NotificacionJudicial> notificacionesJudiciales = (List<NotificacionJudicial>) request.getAttribute("notificacionesJudiciales");
%>

<liferay-portlet:renderURL var="adminNotifications">
	<liferay-portlet:param name="mvcRenderCommandName" value="/notificacionesjudiales/adminNotifications" />
</liferay-portlet:renderURL>


<div class="emcali-notificaciones-judiciales">
	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="table-responsive">
						<table class="table table-striped">
							<thead>
								<tr>
									<th><liferay-ui:message key="emcali.notificacionesjudiciales.user.listado.header.fecha" /></th>
									<th><liferay-ui:message key="emcali.notificacionesjudiciales.user.listado.header.id" /></th>
									<th><liferay-ui:message key="emcali.notificacionesjudiciales.user.listado.header.razon-social" /></th>
									<th><liferay-ui:message key="emcali.notificacionesjudiciales.user.listado.header.acto" /></th>
									<th><liferay-ui:message key="emcali.notificacionesjudiciales.user.listado.header.contrato" /></th>
									<th><liferay-ui:message key="emcali.notificacionesjudiciales.user.listado.header.estado" /></th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<%
									for (NotificacionJudicial notificacionJudicial : notificacionesJudiciales) {
								%>
								<tr>
									<td><%=sdf.format(notificacionJudicial.getActoFecha()) %></td>
									<td><%=notificacionJudicial.getNumeroIdentificacion() %></td>
									<td><%=notificacionJudicial.getNombre() %></td>
									<td><%=notificacionJudicial.getActoNumero() %></td>
									<td><%=notificacionJudicial.getContrato() %></td>
									<td><%=notificacionJudicial.getEstado() %></td>
								</tr>
								<%
									}
								%>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>