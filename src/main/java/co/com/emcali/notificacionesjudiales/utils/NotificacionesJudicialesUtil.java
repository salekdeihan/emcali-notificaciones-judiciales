package co.com.emcali.notificacionesjudiales.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Base64;
import com.liferay.portal.kernel.util.CalendarFactoryUtil;
import com.liferay.portal.kernel.util.Validator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.http.HttpHeaders;

import co.com.emcali.notificacionesjudiales.constants.Constantes;
import co.com.emcali.notificacionesjudiales.model.NotificacionJudicial;

public class NotificacionesJudicialesUtil {
	private static final Log LOGGER = LogFactoryUtil.getLog(NotificacionesJudicialesUtil.class);

	public static boolean isValidUserCredentials(String user, String passs) {

		return true;
	}

	public static boolean generateCode(String tipo, String documento) {

		StringBuilder sb = new StringBuilder();
		String serviceURL = Constantes.URL_CONSULTA_EXTERNOS_GENERATE + "/" + tipo + "/" + documento;

		try {

			URL urlVal = new URL(serviceURL);
			HttpURLConnection conn = (HttpURLConnection) urlVal.openConnection();
			conn.setRequestProperty(HttpHeaders.CONTENT_TYPE, "application/json");
			conn.setRequestProperty(HttpHeaders.ACCEPT, "application/json");
			conn.setRequestMethod("GET");
			conn.setDoOutput(true);
			int responseCode = conn.getResponseCode();
			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			int cp;
			while ((cp = in.read()) != -1) {
				sb.append((char) cp);
			}

			String json = sb.toString();
			LOGGER.info(json);

			if (Validator.isNotNull(json) && json.equals("Mensaje enviado exitosamente")) {
				return true;
			}

		} catch (IOException e) {
			LOGGER.error(e);
		}

		return false;
	}

	public static boolean validateCode(String tipoDocumento, String cedula, String codigo) {

		StringBuilder sb = new StringBuilder();
		String serviceURL = Constantes.URL_CONSULTA_EXTERNOS_VALIDATE + "/" + tipoDocumento + "/" + cedula + "/"
				+ codigo;

		try {

			URL urlVal = new URL(serviceURL);
			HttpURLConnection conn = (HttpURLConnection) urlVal.openConnection();
			conn.setRequestProperty(HttpHeaders.CONTENT_TYPE, "application/json");
			conn.setRequestProperty(HttpHeaders.ACCEPT, "application/json");
			conn.setRequestMethod("GET");
			conn.setDoOutput(true);
			int responseCode = conn.getResponseCode();
			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			int cp;
			while ((cp = in.read()) != -1) {
				sb.append((char) cp);
			}

			String json = sb.toString();
			LOGGER.info(json);

			if (Validator.isNotNull(json) && Boolean.getBoolean(json)) {
				return true;
			}

		} catch (IOException e) {
			LOGGER.error(e);
		}

		return false;
	}

	public static List<NotificacionJudicial> findAll() {

		StringBuilder sb = new StringBuilder();
		List<NotificacionJudicial> notificacionesJudiciales = new ArrayList<NotificacionJudicial>();

		try {

			URL urlVal = new URL(Constantes.URL_CONSUMO_CONSULTA_FINDALL);
			HttpURLConnection conn = (HttpURLConnection) urlVal.openConnection();
			conn.setRequestProperty(HttpHeaders.CONTENT_TYPE, "application/json");
			conn.setRequestProperty(HttpHeaders.ACCEPT, "application/json");
			conn.setRequestMethod("GET");
			conn.setDoOutput(true);
			int responseCode = conn.getResponseCode();
			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			int cp;
			while ((cp = in.read()) != -1) {
				sb.append((char) cp);
			}
			String json = sb.toString();
			Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
			Type notificacionJudicialType = new TypeToken<ArrayList<NotificacionJudicial>>() {
			}.getType();

			notificacionesJudiciales = gson.fromJson(json, notificacionJudicialType);

		} catch (IOException e) {
			LOGGER.error(e);
		}

		return notificacionesJudiciales;
	}

	public static List<NotificacionJudicial> findByNombre(String valorBusqueda) {

		StringBuilder sb = new StringBuilder();
		String serviceURL = Constantes.URL_CONSUMO_CONSULTA_FINDBYNOMBE + "/" + valorBusqueda;
		List<NotificacionJudicial> notificacionesJudiciales = new ArrayList<NotificacionJudicial>();

		try {

			URL urlVal = new URL(serviceURL);
			HttpURLConnection conn = (HttpURLConnection) urlVal.openConnection();
			conn.setRequestProperty(HttpHeaders.CONTENT_TYPE, "application/json");
			conn.setRequestProperty(HttpHeaders.ACCEPT, "application/json");
			conn.setRequestMethod("GET");
			conn.setDoOutput(true);
			int responseCode = conn.getResponseCode();
			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			int cp;
			while ((cp = in.read()) != -1) {
				sb.append((char) cp);
			}

			String json = sb.toString();
			Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
			Type notificacionJudicialType = new TypeToken<ArrayList<NotificacionJudicial>>() {
			}.getType();

			notificacionesJudiciales = gson.fromJson(json, notificacionJudicialType);

		} catch (IOException e) {
			LOGGER.error(e);
		}

		return notificacionesJudiciales;
	}

	public static List<NotificacionJudicial> findByIdentificador(String valorBusqueda) {

		StringBuilder sb = new StringBuilder();
		String serviceURL = Constantes.URL_CONSUMO_CONSULTA_FINDBYIDENTIFICADOR + "/" + valorBusqueda;
		List<NotificacionJudicial> notificacionesJudiciales = new ArrayList<NotificacionJudicial>();

		try {

			URL urlVal = new URL(serviceURL);
			HttpURLConnection conn = (HttpURLConnection) urlVal.openConnection();
			conn.setRequestProperty(HttpHeaders.CONTENT_TYPE, "application/json");
			conn.setRequestProperty(HttpHeaders.ACCEPT, "application/json");
			conn.setRequestMethod("GET");
			conn.setDoOutput(true);
			int responseCode = conn.getResponseCode();
			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			int cp;
			while ((cp = in.read()) != -1) {
				sb.append((char) cp);
			}

			String json = sb.toString();
			Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
			Type notificacionJudicialType = new TypeToken<ArrayList<NotificacionJudicial>>() {
			}.getType();

			notificacionesJudiciales = gson.fromJson(json, notificacionJudicialType);

		} catch (IOException e) {
			LOGGER.error(e);
		}

		return notificacionesJudiciales;
	}

	public static List<NotificacionJudicial> findByDocumento(String tipoDocumento, String documento) {

		StringBuilder sb = new StringBuilder();
		String serviceURL = Constantes.URL_CONSUMO_CONSULTA_FINDBYDOCUMENTO + "/" + tipoDocumento + "/" + documento;
		List<NotificacionJudicial> notificacionesJudiciales = null;

		try {

			URL urlVal = new URL(serviceURL);
			HttpURLConnection conn = (HttpURLConnection) urlVal.openConnection();
			conn.setRequestProperty(HttpHeaders.CONTENT_TYPE, "application/json");
			conn.setRequestProperty(HttpHeaders.ACCEPT, "application/json");
			conn.setRequestMethod("GET");
			conn.setDoOutput(true);
			int responseCode = conn.getResponseCode();
			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			int cp;
			while ((cp = in.read()) != -1) {
				sb.append((char) cp);
			}

			String json = sb.toString();
			Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
			Type notificacionJudicialType = new TypeToken<ArrayList<NotificacionJudicial>>() {
			}.getType();

			notificacionesJudiciales = gson.fromJson(json, notificacionJudicialType);

		} catch (IOException e) {
			LOGGER.error(e);
		}

		return notificacionesJudiciales;
	}

	public static List<NotificacionJudicial> findByNombreAndIdentificador(String valorBusqueda) {
		List<NotificacionJudicial> nj1 = findByNombre(valorBusqueda);
		List<NotificacionJudicial> nj2 = findByIdentificador(valorBusqueda);
		List<NotificacionJudicial> notificacionesJudiciales = new ArrayList<NotificacionJudicial>();

		if (Validator.isNotNull(nj1) && !nj1.isEmpty()) {
			notificacionesJudiciales.addAll(nj1);
		}

		if (Validator.isNotNull(nj2) && !nj2.isEmpty()) {
			notificacionesJudiciales.addAll(nj2);
		}

		return notificacionesJudiciales;
	}

	public static boolean saveNotificacionJudicial(NotificacionJudicial notificacionJudicial) {

		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		String json = gson.toJson(notificacionJudicial);
		HttpURLConnection conn = null;

		try {

			URL urlVal = new URL(Constantes.URL_CONSUMO_CONSULTA_ADD);
			conn = (HttpURLConnection) urlVal.openConnection();
			conn.setRequestProperty(HttpHeaders.CONTENT_TYPE, "application/json");
			conn.setRequestProperty(HttpHeaders.ACCEPT, "application/json");
			conn.setRequestMethod("POST");
			conn.setDoOutput(true);

			OutputStreamWriter dout = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
			LOGGER.info(json);
			dout.write(json);
			dout.flush();
			dout.close();

			int responseCode = conn.getResponseCode();
			LOGGER.info("tk: respuesta " + responseCode);
			return true;
		} catch (IOException exception) {
			LOGGER.error(exception);
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}

		return false;
	}

	public static boolean editNotificacionJudicial(NotificacionJudicial notificacionJudicial) {

		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		String json = gson.toJson(notificacionJudicial);
		HttpURLConnection conn = null;

		try {

			URL urlVal = new URL(Constantes.URL_CONSUMO_CONSULTA_UPDATE);
			conn = (HttpURLConnection) urlVal.openConnection();
			conn.setRequestProperty(HttpHeaders.CONTENT_TYPE, "application/json");
			conn.setRequestProperty(HttpHeaders.ACCEPT, "application/json");
			conn.setRequestMethod("POST");
			conn.setDoOutput(true);

			OutputStreamWriter dout = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
			LOGGER.info(json);
			dout.write(json);
			dout.flush();
			dout.close();

			int responseCode = conn.getResponseCode();

			return true;
		} catch (IOException exception) {
			LOGGER.error(exception);
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}

		return false;
	}

	public static boolean deleteNotificacionJudicial(Integer id) {

		StringBuilder sb = new StringBuilder();
		String serviceURL = Constantes.URL_CONSUMO_CONSULTA_DELETE + "/" + id;
		LOGGER.info(serviceURL);

		try {

			URL urlVal = new URL(serviceURL);
			HttpURLConnection conn = (HttpURLConnection) urlVal.openConnection();
			conn.setRequestMethod("GET");
			conn.setDoOutput(true);
			int responseCode = conn.getResponseCode();
			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			int cp;
			while ((cp = in.read()) != -1) {
				sb.append((char) cp);
			}

			String json = sb.toString();
			LOGGER.info(json);
			return true;

		} catch (Exception e) {
			LOGGER.error(e);
		}

		return false;

	}

	public static NotificacionJudicial findNotificacionJudicialById(Integer id) {

		StringBuilder sb = new StringBuilder();
		String urlService = Constantes.URL_CONSUMO_CONSULTA_FINDBYID;
		urlService = urlService + "/" + id;

		try {

			URL urlVal = new URL(urlService);
			HttpURLConnection conn = (HttpURLConnection) urlVal.openConnection();
			conn.setRequestProperty(HttpHeaders.CONTENT_TYPE, "application/json");
			conn.setRequestProperty(HttpHeaders.ACCEPT, "application/json");
			conn.setRequestMethod("GET");
			conn.setDoOutput(true);
			int responseCode = conn.getResponseCode();
			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			int cp;
			while ((cp = in.read()) != -1) {
				sb.append((char) cp);
			}

			String json = sb.toString();

			Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
			Type ossNotificationType = new TypeToken<NotificacionJudicial>() {
			}.getType();

			NotificacionJudicial notificacionJudicial = gson.fromJson(json, ossNotificationType);
			return notificacionJudicial;

		} catch (IOException e) {
			LOGGER.error(e);
		}

		return null;
	}

	public static String getBase64File(File file) {

		try {
			byte[] bytesArray = new byte[(int) file.length()];

			FileInputStream fis = new FileInputStream(file);
			fis.read(bytesArray);
			fis.close();

			Base64 base64 = new Base64();
			String encodedString = new String(base64.encode(bytesArray));
			return encodedString;

		} catch (IOException e) {
			LOGGER.error(e);
		}

		return "";
	}

	public static boolean isNotificacionUnaSemana(Date actoFecha) {

		Calendar calendarWeek = CalendarFactoryUtil.getCalendar();

		calendarWeek.add(Calendar.DAY_OF_YEAR, 7);

		Calendar calendarActo = CalendarFactoryUtil.getCalendar();
		calendarActo.setTime(actoFecha);

		if (calendarWeek.getTime().getTime() >= actoFecha.getTime()) {
			return true;
		}

		return false;
	}
}
